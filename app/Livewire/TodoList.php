<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Attributes\Rule;
use App\Models\Todo;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
class TodoList extends Component
{
    use WithPagination;
    use WithFileUploads;

    #[Rule('required|min:3|max:50')]
    public $name;

    public  $search;
    public $EditingTodoID;

    #[Rule('required|min:3|max:50')]
    public  $EditingTodoName;

    // #[Rule('nullable|sometimes|image|max:10240')]
    public $image;

    public function create(){
        // dd('pauro');
        sleep(3);
        // validate
        $validated = $this->validateOnly('name');
         // ?create the todo

         if($this->image){
           $validated['image'] = $this->image->store('',  'pauro');
         }
        Todo::create($validated);

      
        // clear input
        $this->reset('name');

        // send flash message
        request()->session()->flash('success', 'successfully saved todo');

        $this->resetPage();

    }
// delete method
    public function delete($todo)
    {
        try{
            Todo::findOrfail($todo)->delete();
        }catch(\Exception $e){
            // dd($e);
           session()->flash('error', 'failed to delete todo');
            return;

        }
        
    }

    public function toggle($todo){
        
            $tod_o= Todo::find($todo);
            // dd($tod_o->completed);
            $tod_o->completed = !$tod_o->completed;
            $tod_o->save();
    }

    public function edit($todo){
        try{
            $this->EditingTodoID = $todo;
            $this->EditingTodoName = Todo::find($todo)->name;
        }catch(\Exception $e){
            dd('pauro');
        }
        
    }
    
    public function update(){
        $this->validateOnly('EditingTodoName');
        Todo::find($this->EditingTodoID)
        ->update(
            ['name'=>$this->EditingTodoName
            ]
        );
        request()->session()->flash('edited', 'Edited successfully' );
        $this->reset('EditingTodoID');
        $this->reset('EditingTodoName');

        
        
    }

    public function cancelEdit(){
        $this->reset('EditingTodoID');
        $this->reset('EditingTodoName');

    }

    public function render()
    {
        $todos = Todo::latest()
            ->where('name', 'like', "%{$this->search}%")
            ->paginate(5);
        $data = [
            'todos'=>$todos
        ];
        return view('livewire.todo-list', $data );
    }
}
