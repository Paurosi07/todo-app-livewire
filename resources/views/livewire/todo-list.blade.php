<div>
    @if(session('error'))
        <div class="p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400" role="alert">
            <span class="font-medium">{{session("error")}}</span> 
        </div>
        <!-- {{session('error')}} -->
    @endif
    <!-- {{-- Care about people's approval and you will be their prisoner. --}} -->
        @include('livewire.includes.create-todo-box')
        
       @include('livewire.includes.search-box')
        <div id="todos-list">
                @if(session('edited'))
                                   <span class="text-green-500 text-xs">{{session('edited')}}</span>
                                @endif
            @foreach($todos as $todo)
                @include('livewire.includes.todo-card')
            @endforeach
            
            

            <div class="my-2">
                <!-- Pagination goes here -->
                {{$todos->links()}}
            </div>
        </div>

        
</div>
