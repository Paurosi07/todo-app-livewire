<div class="container content py-6 mx-auto">
            <div class="mx-auto">
                <div id="create-form" class="hover:shadow p-6 bg-white border-blue-500 border-t-2">
                    <div class="flex ">
                        <h2 class="font-semibold text-lg text-gray-800 mb-5">Create New Todo</h2>
                    </div>
                    <div>
                        <form>
                            <div class="mb-6">
                                <label for="name"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">*
                                    Todo </label>
                                <input wire:model="name" type="text" id="name" placeholder="Todo.."
                                    class="bg-gray-100  text-gray-900 text-sm rounded block w-full p-2.5">

                                    @error('name')
                                         <span class="text-red-500 text-xs mt-3 block ">{{$message}}</span>
                                    @enderror
                                    <input wire:model="image" type="file" id="image" placeholder="profile pic"
                                    class="bg-gray-100  mt-2 text-gray-900 text-sm rounded block w-full p-2.5">

                                    @error('image')
                                         <span class="text-red-500 text-xs mt-3 block ">{{$message}}</span>
                                    @enderror
                                    @if($image)
                                        <img class ="rounded w-10 h-10 mt-5 block" src='{{$image->temporaryUrl() }}'>
                                    @endif

                                    <div wire:loading wire:target="image">
                                        <span class="text-green-500">Uploading </span>
                                    </div>

                            </div>
                            <button wire:click.prevent="create" wire:loading.attr="disabled" wire:loading.class="bg-red-500" type="submit"
                                class="px-4 py-2 bg-blue-500 text-white font-semibold rounded hover:bg-blue-600">Create
                                +</button>
                                @if(session('success'))
                                   <span class="text-green-500 text-xs">{{session('success')}}</span>
                                @endif
                            

                        </form>
                    </div>
                </div>
        </div>
</div>